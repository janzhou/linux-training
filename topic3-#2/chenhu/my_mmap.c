#include <linux/module.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <asm/io.h>
#include <asm/system.h>
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ch");

static int my_major = 0;
#define CDEV_SIZE 0X1000

struct mmap_cdev
{
    char *data;
    unsigned long size;
};
static struct mmap_cdev *cdevp;
struct cdev mycdev;

//open
    int
mmap_open(struct inode *inode, struct file *filp)
{
    filp->private_data =cdevp;
    return 0;
}
//release
    int 
mmap_release(struct inode *inode, struct file *filp)
{
    return 0;
}

//write
    static ssize_t
mmap_write (struct file *filp, const char __user * buf, size_t count,
        loff_t *f_pos)
{return 0;
}
//read
    static ssize_t
mmap_read (struct file * filp, const char __user * buf, size_t count,
        loff_t *f_pos)
{return 0;
}
//mmap
    static int
mmap(struct file *filp, struct vm_area_struct *vma)
{
    struct mmap_cdev *dev = filp->private_data;
    vma->vm_flags |= VM_IO;
    vma->vm_flags |= VM_RESERVED;
    if(remap_pfn_range(vma,vma->vm_start,virt_to_phys(dev->data)>>PAGE_SHIFT, vma->vm_end - vma->vm_start, vma->vm_page_prot))
        return -EAGAIN;

    return 0;   
}

static const 
struct file_operations my_fops =
{
    .owner = THIS_MODULE,
    .open = mmap_open,
    .release = mmap_release,
    .write=mmap_write,
    .read=mmap_read,
    .mmap = mmap,
};

    static int 
map_init(void)
{
    int result;

    dev_t devno = MKDEV(my_major, 0);
    if (my_major)
        result = register_chrdev_region(devno, 1, "mmapdev");
    else
    {
        result = alloc_chrdev_region(&devno, 0, 1, "mmapdev");
        my_major = MAJOR(devno);
    }

    cdev_init(&mycdev, &my_fops);
    mycdev.owner = THIS_MODULE;
    mycdev.ops = &my_fops;

    cdev_add(&mycdev, MKDEV(my_major, 0), 1);

    cdevp = kmalloc(sizeof(struct mmap_cdev), GFP_KERNEL);
    if (!cdevp)
    {
        result = -ENOMEM;
        goto fail_malloc;
    }
    memset(cdevp, 0, sizeof(struct mmap_cdev));

    cdevp[0].size = CDEV_SIZE;
    cdevp[0].data = kmalloc(CDEV_SIZE, GFP_KERNEL);
    memset(cdevp[0].data, 0, CDEV_SIZE);

    return 0;

fail_malloc:
    unregister_chrdev_region(devno, 1);

    return result;
}

static void map_exit(void)
{

    cdev_del(&mycdev);
    kfree(cdevp);
    unregister_chrdev_region(MKDEV(my_major, 0), 1);
}

module_init(map_init);
module_exit(map_exit);

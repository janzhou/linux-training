#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/mm.h>
#include <linux/gfp.h>
#include <linux/slab.h>

static int Major;
static int Minor;

#ifndef DEV_MAJOR
#define DEV_MAJOR 249   //预设的主设备号
#endif

static int my_major = DEV_MAJOR;

struct memcdev {
	int mem_var;
	struct cdev cdev;
} *memcdev_dev;                //虚拟出的字符设备

static char *share_mem;       //全局变量，指向共享内存区域的指针，供内核程序在操作此共享内存时使用
struct page *share_page;      //指向共享内存中起始页面的page结构
#define SHARE_SIZE 1          //共享内存区域的大小，以页为单位

/**********************************************************************/

static int memcdev_open(struct inode *inode, struct file *file)
{
	Minor = MINOR(inode->i_rdev);
	Major = MAJOR(inode->i_rdev);
	
	//dev_t i_rdev; 设备文件的inode结构,该字段表示真正的设备编号
	printk("Open_Device memcdev_dev: %d.%d\n", Major, Minor);
	return 0;
}

static int memcdev_release(struct inode *inode, struct file *file)
{
	printk("Release_Device memcdev_dev: %d.%d\n", Major, Minor);
	return 0;
}

static ssize_t memcdev_read(struct file *file, char *buffer, size_t length, loff_t *offset)
{
	printk("Read_Device memcdev_dev: %d.%d\n", Major, Minor);
	
	//////////////////////////////////////////
	strcpy(buffer,share_mem);
	return 0;
}

static ssize_t memcdev_write(struct file *file, const char *buffer, size_t length, loff_t *offset) 	
{
	printk("Write_Device memcdev_dev: %d.%d\n", Major, Minor);
	
	////////////////////////////////////////////
	strcpy(share_mem,buffer);
	return 0;
}

 /*****************************************************************/
 
 //对VMA函数open和close的调用由内核处理
 //VMA：虚拟映射区，用于管理进程地址空间中不同区域的内核数据结构，符合更宽泛的“段”的概念
 
void memcdev_vma_open(struct vm_area_struct *vma)
 {
	printk(KERN_NOTICE "Memcdev VMA open, virt %lx, phys %lx\n",
	       vma->vm_start, vma->vm_pgoff << PAGE_SHIFT);
 }
 
void memcdev_vma_close(struct vm_area_struct *vma)
{
	printk(KERN_NOTICE "Memcdev VMA close.\n");
} 

static struct vm_operations_struct memcdev_remap_vm_ops = {
	.open = memcdev_vma_open,
	.close = memcdev_vma_close,
};

/*************************************************************/

//当用户空间使用系统调用mmap操作我们的设备文件时，最终会执行到mmap。
//函数remap_pfn_rang负责为一段物理地址建立新的页表。
//page_to_pfn函数将表示物理页面的page结构转换为其对应的页帧号。
static int memcdev_mmap(struct file *filp, struct vm_area_struct *vma)
{
	printk("memcdev_mmap is called.\n");
	
	if( remap_pfn_range(vma, vma->vm_start, page_to_pfn(share_page), 
	                    vma->vm_end - vma->vm_start,
 						vma->vm_page_prot) )
		return -EAGAIN;
		
		vma->vm_ops = &memcdev_remap_vm_ops;
		memcdev_vma_open(vma);
		return 0;
}

/*******************************************************************/

struct file_operations memcdev_fops = {
	.owner = THIS_MODULE,
	.open = memcdev_open,
	.release = memcdev_release,
	.read = memcdev_read,
	.write = memcdev_write,
//	.unlocked_ioctl = memcdev_ioctl,
	.mmap = memcdev_mmap,
 };

 
 /***************************************************************/
 
//模块的初始化和模块的卸载
//这个函数用来初始化这个模块 —注册该字符设备
//init_module()函数调用module_register_chrdev，把设备驱动程序添加到内核的字符设备驱动程序表中，它返回这个驱动程序所使用的主设备号。
int memcdev_init(void)
{
	int result;
	int err;
	
	dev_t my_dev = MKDEV(my_major, 0);
	
	//静态申请设备号
	result = register_chrdev_region(my_dev, 1, "memcdev");
	
	// 静态分配失败，动态分配设备号	
	if (result < 0)          
	{
		result = alloc_chrdev_region(&my_dev, 0, 1, "memcdev");
		my_major = MAJOR(my_dev);
	}  
	
	printk("alloc successfully memcdev:%d(MAJOR)\n", my_major);
	if (result < 0)
		return result;
	
	memcdev_dev = kmalloc( sizeof(struct memcdev), GFP_KERNEL );
	
	if( !memcdev_dev )
	{
		result = -ENOMEM;
		printk("create device failed.\n");
	}
	else
	{
		//初始化memcdev结构
		memcdev_dev->mem_var = 0;
		cdev_init( &memcdev_dev->cdev, &memcdev_fops);
		memcdev_dev->cdev.owner = THIS_MODULE;
		
		//注册字符设备 
		err = cdev_add( &memcdev_dev->cdev, my_dev,1 );
		if(err)
			printk(KERN_NOTICE "Error %d adding memcdev",err);  
	}
	
	//申请需要的页面
	share_page = alloc_pages(__GFP_WAIT, SHARE_SIZE);
	//函数page_address将page结构转换成内核中可以直接访问的线性地址。
	//share_mem此时便指向了我们刚申请的内存区域的起始地址。可以对其进行直接读写操作。
	share_mem = page_address(share_page);
	
    //////strcpy(share_mem,"hello, mmap\n");
	
	return result;
}

 
//这个函数的功能是卸载模块，主要是从/proc中取消注册的设备特殊文件。
void memcdev_exit()
{
	cdev_del(&memcdev_dev->cdev);                                 //注销设备   /////这里需要吗？
	unregister_chrdev_region(MKDEV(my_major,0),1);          //释放设备号
	printk("unregister successfully memcdev.\n");
	kfree(memcdev_dev);
}

module_init(memcdev_init);
module_exit(memcdev_exit);

MODULE_LICENSE("Dual BSD/GPL");







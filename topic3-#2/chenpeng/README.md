程序功能说明
============
 - 驱动程序simple.c实现了用remap_pfn_range和nopage两种建立页表的方式。
 - mmap将用户空间地址映射到字符设备内存上。测试程序test.c读写用户空间地址时，实际上是在访问设备。

测试流程
========
	make
	make install
	make test
	make uninstall
	make clean

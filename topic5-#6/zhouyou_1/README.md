#内核读写文件一般有两种方法

##1. 利用系统调用

    sys_open,sys_write,sys_read,sys_close
	
通过分析sys_open可以知道，它最后调用的也是filp->open。

    sys_open ==> do_sys_open ==> filp->open

sys_write,sys_read等调用没有导出到内核，所以不能使用。

sys_open 也在内核版本2.6.25以后不再被支持。

该网址说明最好不要使用sys_write,sys_read等调用。

    http://kernelnewbies.org/FAQ/WhyWritingFilesFromKernelIsBad	


##2. filp->open(read,write,llseek,close)等函数

在模块中，用户空间的open,read,write,llseek等函数都是不可以使用的。

filp->read(write)声明中有参数 char __user* buffer，__user修饰符说明buffer指针应该指向用户的内存，如果对该参数传递kernel空间的指针，这两个函数都会返回失败-EFAULT。

要使这两个读写函数使用kernel空间的buffer指针也能正确工作，需要使用set_fs()函数或宏(set_fs()可能是宏定义)。

其原型如下：void set_fs(mm_segment_t fs);

该函数的作用是改变kernel对内存地址检查的处理方式，其实该函数的参数fs只有两个取值：USER_DS，KERNEL_DS，分别代表用户空间和内核空间，默认情况下，kernel取值为USER_DS，即对用户空间地址检查并做变换。


##倘若使用sys_open,sys_read,sys_write,sys_close

编译通过，但在insmod模块的时候，提示

    insmod: error inserting 'kernel_file_sys.ko': -1 Unknown symbol in module

查看dmesg：

    [ 6419.207355] kernel_file_sys: Unknown symbol sys_read (err 0)
	
    [ 6419.207365] kernel_file_sys: Unknown symbol sys_open (err 0)
	
    [ 6419.207367] kernel_file_sys: Unknown symbol sys_write (err 0)
	
#示例程序使用方法

##1. 编译 

    make

##2. 加载模块

    make install

##3. 查看系统日志，检查输出

    dmesg
		
    [28363.052438] The File Content is:
    
	[28363.052447] hello, u got me.

##4. 卸载模块

    make uninstall

##5. 清除相关文件

    make clean
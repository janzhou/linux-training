/*
内核读写文件一般可以用两种方法：第一种是用系统调用。第二种方法是filp->open()等函数。

1 利用系统调用：
sys_open,sys_write,sys_read等。
其实分析过sys_open可以知道，最后调用的也是filp->open。
sys_open ==> do_sys_open ==> filp->open

http://kernelnewbies.org/FAQ/WhyWritingFilesFromKernelIsBad	
sys_write,sys_read没有导出到内核，所以不能使用。
sys_open 在内核版本2.6.25以后不再被支持。

2 filp->open等函数。
在模块中，用户空间的open,read,write,llseek等函数都是不可以使用的。
可以使用filp->open(read,write,llseek)配合struct file里的read/write来进行对文件的读写操作。

	fget(): 根据文件描述符(int型，sys_open的返回值)得到其对应的struct file结构,
	        并且给struct file结构的f_count引用数+1； 
	fput(): 与fget()对应，对f_count进行-1，如果发现f_count为0了，
	        那么将其对应的struct file结构删除。
*/


#include<linux/module.h>  
#include<linux/kernel.h>  
#include<linux/init.h>  
#include<linux/types.h>  
#include<linux/fs.h>  
#include<linux/string.h>  
#include<asm/uaccess.h>       //get_fs(),set_fs(),get_ds()
#include<linux/slab.h>        //kmalloc 
//#include<linux/syscalls.h>    //sys_open, sys_read, sys_close
#include<linux/fcntl.h>
//#include<linux/file.h>       //vfs_read,vfs_write
 
#define FILE_DIR "/tmp/test_filp"

MODULE_LICENSE("Dual BSD/GPL");  

static int my_init(void)  
{  
    struct file *filp;       //文件指针
	struct inode *inode;     //索引节点
	
	/*
	mm_segment_t 的原型 (Linux/arch/frv/include/asm/segment.h)
	typedef struct {
          unsigned long seg;
    } mm_segment_t;
	*/
	mm_segment_t fs;         
	off_t fsize;   
	
	char w_buf[] = "hello, u got me.\n";
	char *r_buf;
	
	filp = filp_open(FILE_DIR, O_RDWR | O_CREAT, 0); 
	if( IS_ERR(filp) )
	{
		printk("error occured while opening file.\n");
		return -1;
	}
	
	
	fs = get_fs();   
	//改变kernel对内存地址检查的处理方式，该函数的参数只有两个取值
	//USER_DS，KERNEL_DS，分别代表用户空间和内核空间，默认情况下，kernel取值为USER_DS
	set_fs(KERNEL_DS);
	
	//向文件写入
	filp->f_op->write(filp, w_buf, sizeof(w_buf), &(filp->f_pos)); 
	
	//或者 loff_t pos = 0;  vfs_write(filp, w_buf, sizeof(w_buf), &pos);
	
	inode = filp->f_dentry->d_inode;    
	fsize = inode->i_size;
	  
	//kmalloc 能够分配的内存块的大小有一个上限(128KB-16)，kmalloc特殊之处在于它分配的内存是物理上连续的
	r_buf = (char *) kmalloc(fsize+1,GFP_ATOMIC);

	//写完文件时文件指针指到了末尾，需要手动将指针移到文件头
	filp->f_op->llseek(filp, 0, 0);
	//读取文件内容
	filp->f_op->read(filp, r_buf, fsize, &(filp->f_pos));
	
	//或者 pos = 0;   vfs_read(filp, r_buf, sizeof(w_buf), &pos );
	
	//改变kernel对内存地址检查的处理方式为用户空间
	set_fs(fs);   
	r_buf[fsize]='\0';   
  
	printk("The File Content is:\n");   
	printk("%s", r_buf);   
  
	filp_close(filp, NULL);   
    return 0;  
}  
  
static void my_exit(void)  
{  
        printk("Goodbye!\n");  
}  
  
module_init(my_init);  
module_exit(my_exit);  

MODULE_AUTHOR("zy");
MODULE_DESCRIPTION("kernel_file_ops");
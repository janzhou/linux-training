#include<linux/kernel.h>
#include<linux/module.h>
#include<linux/stat.h>
#include<linux/fs.h>
#include<asm/unistd.h>
#include<asm/uaccess.h>
#include<linux/types.h>
#include<linux/ioctl.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Chen Hu");

#define path "/var/log/messages"
static char buf1[80];
static char buf2[80];

static int sys_rw_init(void){
    mm_segment_t old_fs;
    ssize_t result;
    ssize_t ret;
    loff_t pos;
    sprintf(buf1,"%s","hello,world!");
    struct file *file=NULL;
    file=filp_open(path,O_RDWR,0);
    if(IS_ERR(file))goto fail;
    old_fs=get_fs();
    set_fs(get_ds());
    pos=0; 
    ret=file->f_op->write(file,buf1,sizeof(buf1),&pos); 
    if(ret<0){
        printk("write error\n");
    }
    pos=0;
    result=file->f_op->read(file,buf2,sizeof(buf2),&pos);
    if(result<0){
        printk("read error\n");
    }
    if(result>=0){
        buf2[18]='\n';
        printk("buf2-->%s\n",buf2);
    } else {
        printk("failed\n");
    }
    set_fs(old_fs);
    filp_close(file,NULL);
    printk("file loaded\n");
    return 0;
fail:{
         filp_close(file,NULL);
         printk("load failed\n");
         return 1;
     }

}

static void __exit sys_rw_cleanup(void){
    printk("moudle exit\n");
}

module_init(sys_rw_init);
module_exit(sys_rw_cleanup);

程序功能
========
***
- 本次培训主要是利用**netlink**实现用户态与内核态的通信
- nl_user.c为netlink通信用户态程序
- nl_kernel.c为netlink通信内核态程序
***

测试流程
========
	make
	make install
	make test
	dmesg	查看内核打印信息
	make uninstall
	make clean

#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <asm/types.h>
#include <linux/netlink.h>
#include <linux/socket.h>

#define MAX_MSGSIZE 1024  /*消息最大负载为1024个字节*/

int main(int argc, char* argv[])
{
        struct sockaddr_nl src_addr, dest_addr;
        struct nlmsghdr *nlh = NULL;
        struct iovec iov;
        int sock_fd = -1;
        struct msghdr msg;
		
		//生成一个AF_NETLINK族套接字
		//第一个参数指定通信协议的协议族，第二个参数指定套接字类型，第三个参数指定程序所使用的通信协议
        if((sock_fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_TEST)) == -1){//创建套接字
                perror("Can't create netlink socket\n");
                return -1;
        }
		
        memset(&src_addr, 0, sizeof(src_addr));
        src_addr.nl_family = AF_NETLINK;
        src_addr.nl_pid = getpid();
        src_addr.nl_groups = 0;

        //将套接字和netlink地址结构(sockaddr)绑定
        if(-1 == bind(sock_fd, (struct sockaddr *)&src_addr, sizeof(src_addr))){
                perror("Can't bind sockfd with sockaddr_nl\n");
                return -1;
        }
                
        memset(&dest_addr, 0, sizeof(dest_addr));
        dest_addr.nl_family = AF_NETLINK;
        dest_addr.nl_pid = 0;       //接收方是内核
        dest_addr.nl_groups = 0;
        
        nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_MSGSIZE));
        if(nlh == NULL){
                perror("alloc mem failed\n");
                return -1;
        }

        memset(nlh, 0, MAX_MSGSIZE);
        //填充netlink消息头部
        nlh->nlmsg_len = NLMSG_SPACE(MAX_MSGSIZE);
		//我们希望得到内核回应，所以得告诉内核我们ID号
        nlh->nlmsg_pid = getpid();
		//指明我们的Netlink是消息负载是一条空消息
        nlh->nlmsg_type = NLMSG_NOOP;
        nlh->nlmsg_flags = 0;
        
        /*
        printf("========================================\n");
        printf("NLMSG_LENGTH(MAX_MSGSIZE) = %d \n", NLMSG_LENGTH(MAX_MSGSIZE));
        printf("NLMSG_SPACE(MAX_MSGSIZE)  = %d \n", NLMSG_SPACE(MAX_MSGSIZE));
        printf("========================================\n");
        */
		
        //设置消息内容
        strcpy(NLMSG_DATA(nlh), argv[1]);

        memset(&iov, 0 ,sizeof(iov));
        iov.iov_base = (void *)nlh;
        iov.iov_len = nlh->nlmsg_len;
        memset(&msg, 0, sizeof(msg));
        msg.msg_name = (void *)&dest_addr;
        msg.msg_namelen = sizeof(dest_addr);
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;

        //通过netlink socket 向内核发送消息
		sendmsg(sock_fd, &msg, 0);

        //接收内核消息
        printf("waiting message from kernel:\n");
        memset((char *)NLMSG_DATA(nlh), 0, MAX_MSGSIZE);
        recvmsg(sock_fd, &msg, 0);
        printf("Got response from kernel: %s\n\n", NLMSG_DATA(nlh));

        close(sock_fd);//关闭netlink套接字
        free(nlh);
        return 0;
        
}
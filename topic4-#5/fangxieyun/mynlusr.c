#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <linux/netlink.h>
#include <linux/socket.h>

#define NETLINK_MYTEST 20
#define MAX_PAYLOAD 1024

int main(int argc, char ** argv)
{
	struct sockaddr_nl src_addr,dest_addr;
	struct nlmsghdr *nlh = NULL;
	struct msghdr msg;
	struct iovec iov;
	int sock_fd = -1;
	/*创建套接字*/
	if(-1 == (sock_fd = socket(AF_NETLINK,SOCK_RAW,NETLINK_MYTEST))){
		printf("can't create netlink socket!\n");
		return -1;
	}
	/*设置src_addr结构*/
	memset(&src_addr, 0, sizeof(src_addr));
	src_addr.nl_family = AF_NETLINK;
	src_addr.nl_pid = getpid();/*自己的pid*/
	src_addr.nl_groups = 0;
	
	/*将套接字和netlink地址结构进行绑定*/
	if(-1 == bind(sock_fd, (struct sockaddr*)&src_addr, sizeof(src_addr))){
		printf("can't bind sock_fd with sockaddr_nl");
		return -1;
	}
	if(NULL == (nlh=(struct nlmsghdr*)malloc(NLMSG_SPACE(MAX_PAYLOAD)))){
		printf("alloc mem failed!\n");
		return -1;
	}
	memset(nlh, 0, MAX_PAYLOAD);
	/*填充netlink的消息头部*/
	nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
	nlh->nlmsg_pid = getpid();
	nlh->nlmsg_type = NLMSG_NOOP;
	nlh->nlmsg_flags = 0;
	
	/*设置netlink的消息内容*/
	strcpy(NLMSG_DATA(nlh), argv[1]);
	
	memset(&iov, 0, sizeof(iov));
	iov.iov_base = (void *)nlh;
	iov.iov_len = nlh->nlmsg_len;
	
	/*设置dest_addr结构*/
	memset(&dest_addr, 0, sizeof(dest_addr));
	dest_addr.nl_family = AF_NETLINK;
	dest_addr.nl_pid = 0;/*目的地址的pid，这里是发送给内核*/
	dest_addr.nl_groups = 0;
	
	memset(&msg, 0, sizeof(msg));
	msg.msg_name = (void *)&dest_addr;
	msg.msg_namelen = sizeof(dest_addr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	
	/*通过netlink socket向内涵发送消息*/
	sendmsg(sock_fd, &msg, 0);
	
	/*接受内核消息*/
	printf("waiting message from kernel!\n");
	memset((char *)NLMSG_DATA(nlh),0,1024);
	recvmsg(sock_fd,&msg,0);
	printf("response:%s\n",NLMSG_DATA(nlh));
	
	/*关闭netlink套接字*/
	close(sock_fd);
	free(nlh);
	return 0;
}


#include "mynull.h"
#include "myzero.h"
#include "mytmp.h"

MODULE_AUTHOR("Chen Peng");
MODULE_LICENSE("Dual BSD/GPL");

static int __init task4_init(void)
{
	return mynull_init() || myzero_init() || mytmp_init();
}

static void __exit task4_exit(void)
{
	mynull_exit();
	myzero_exit();
	mytmp_exit();
}

module_init(task4_init);
module_exit(task4_exit);
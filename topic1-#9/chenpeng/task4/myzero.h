#ifndef MYZERO_H
#define MYZERO_H
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>

#define MYZERO_MAJOR 150
#define ZERO_DEV_NAME "myzero"

int __init myzero_init(void);
void __exit myzero_exit(void);
#endif
#include "mynull.h"

static int mynull_major = MYNULL_MAJOR;
static struct cdev mynull_cdev;

static int open_null(struct inode *inode, struct file *filp);
static int release_null(struct inode *innode, struct file *filp);
static loff_t lseek_null(struct file *filp, loff_t offset, int orig);
static ssize_t read_null(struct file *filp, char __user *buf, size_t count, loff_t *ppos);
static ssize_t write_null(struct file *filp, const char __user *buf, size_t count, loff_t *ppos);

static struct file_operations mynull_fops = {
	.owner = THIS_MODULE,
	.open = open_null,
	.release = release_null,
	.llseek = lseek_null,
	.read = read_null,
	.write = write_null,
};

static int open_null(struct inode *inode, struct file *filp)
{
	printk("Mynull opened!\n");
	return 0;
}

static int release_null(struct inode *innode, struct file *filp)
{
	printk("Mynull released!\n");
	return 0;
}

static loff_t lseek_null(struct file *filp, loff_t offset, int orig)
{
	printk("Mynull lseek!\n");
	return filp->f_pos = 0;
}

static ssize_t read_null(struct file *filp, char __user *buf, size_t count, loff_t *ppos)
{
	printk("Mynull read!\n");
	return -1;
}	

static ssize_t write_null(struct file *filp, const char __user *buf, size_t count, loff_t *ppos)
{
	printk("Mynull write!\n");
	return count;
}

int __init mynull_init(void)
{
	int result;
	dev_t devno;	
	/*
	if(mynull_major){
		devno = MKDEV(mynull_major, 0);
		result = register_chrdev_region(devno, 1, NULL_DEV_NAME);
	}
	else{
		result = alloc_chrdev_region(&devno, 0, 1, NULL_DEV_NAME);
		mynull_major = MAJOR(devno);
	}
	if(result < 0){
		printk("Can't get major devno:%d \n",mynull_major);
		return result;
	}
	*/
	devno = MKDEV(mynull_major, 0);
	result = register_chrdev_region(devno, 1, NULL_DEV_NAME);
	if(result < 0){
		result = alloc_chrdev_region(&devno, 0, 1, NULL_DEV_NAME);
		mynull_major = MAJOR(devno);
	}
	if(mynull_major < 0){
		printk("Cann't get major devno!\n");
		return mynull_major;
	}
	else
		printk("The major devno of mynull is %d\n", mynull_major);
		
	cdev_init(&mynull_cdev, &mynull_fops);
	mynull_cdev.owner = THIS_MODULE;
	mynull_cdev.ops = &mynull_fops;
	cdev_add(&mynull_cdev, MKDEV(mynull_major, 0), 1);
	printk("mynull_init!\n");
	return result;
}

void __exit mynull_exit(void)
{
	cdev_del(&mynull_cdev);
	unregister_chrdev_region(MKDEV(mynull_major, 0), 1);
	printk("mynull exit!\n");
}

//module_init(mynull_init);
//module_exit(mynull_exit);

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

int main()
{
	int fd;
	char s[15] = "helloworld";
	char out[15]="";
	int len = strlen(s);
	int i;
	fd = open("/dev/mynull", O_RDWR, S_IRUSR|S_IWUSR);
	if(fd != -1){
		i = write(fd, s, len);
		if(len == i)
			printf("write success\n");
		i = read(fd, out, len);
		if(len == i)
			printf("read success\n");
		else
			printf("read error\n");
		close(fd);
	}
	else
		printf("device cannot open\n");
	return 0;
}
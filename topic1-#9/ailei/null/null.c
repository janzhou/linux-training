#include<linux/init.h>
#include<linux/module.h>
#include<linux/types.h>
#include<linux/fs.h>
#include<linux/errno.h>
#include<linux/cdev.h>
#include<linux/kernel.h>
#include<linux/slab.h>

MODULE_LICENSE("GPL");

static int null_major = 0;
module_param(null_major, int, 0);

static struct cdev nullDev;

int null_open(struct inode *inode, struct file *filp)
{
	//printk("null_open here.\n");
	return 0;
}

int null_release(struct inode *inode, struct file *filp)
{
	//printk("null_release here.\n");
	return 0;
}

static ssize_t null_read(struct file *filp, char __user *buf, size_t size, loff_t *ppos)
{
	int ret = -1;
	//printk("null_read here.\n");
	
	return ret;
}

static ssize_t null_write(struct file *filp, const char __user *buf, size_t size, loff_t *ppos)
{
	int ret = 1;
	//printk("null_write here.\n");
	printk("write %d bytes.\n", size);
	return ret;
}

static const struct file_operations fops = {
	.owner		= THIS_MODULE,
	.open		= null_open,
	.release	= null_release,
	.read		= null_read,
	.write		= null_write,
};
	
static int null_init(void)
{
	int ret, devno;
	dev_t dev = MKDEV(null_major, 0);
	//printk("null_init here.\n");
	if (null_major)
		ret = register_chrdev_region(dev, 1, "null");
	else {
		ret = alloc_chrdev_region(&dev, 0, 1, "null");
		null_major = MAJOR(dev);
	}
	if (ret < 0) {
		printk("null: unable to get major %d.\n", null_major);
		return ret;
	}
	
	devno = MKDEV(null_major, 0);
	cdev_init(&nullDev, &fops);
	nullDev.owner = THIS_MODULE;
	nullDev.ops = &fops;
	ret = cdev_add(&nullDev, devno, 1);
	if (ret) {
		printk("null: error %d adding null.\n", ret);
	}
	
	return 0;
}

static void null_exit(void)
{
	//printk("null_exit here.\n");
	cdev_del(&nullDev);
	unregister_chrdev_region(MKDEV(null_major, 0), 1);
}

module_init(null_init);
module_exit(null_exit);
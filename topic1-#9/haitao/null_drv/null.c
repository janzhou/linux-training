#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>
MODULE_LICENSE("GPL");

int null_major=100;
int null_minor=0;
int number_of_devices=1;

struct cdev *cdev;
dev_t dev=0;

static int null_open(struct inode *inode,struct file *file)
{
	printk(KERN_INFO "My_null device opened!\n");
	return 0;
}

void null_read(struct file *file,char *buff, size_t len, loff_t *pOff)
{
	//doing nothing because I'm NULL
}
 
ssize_t null_write(struct file *file,const char *buff,size_t len,loff_t *pOff)
{
	return len;
}

static int null_release(struct inode *inode, struct file *file)
{
	printk(KERN_INFO "null device closed!\n");
	return 0;
}

struct file_operations null_fops=
{
	.owner=THIS_MODULE,
	.open=null_open,
	.read=null_read,
	.write=null_write,
	.release=null_release,
};

static int __init null_init(void)
{
	int result;
	dev=MKDEV(null_major,null_minor);	
	result=register_chrdev_region(dev,number_of_devices,"null_drv");
	if(result<0)
	{
		printk(KERN_WARNING "Cannot get major number %d\n",null_major);
	}

	cdev=cdev_alloc();
	cdev_init(cdev,&null_fops);
	cdev->owner=THIS_MODULE;
	cdev->ops=&null_fops;
	result=cdev_add(cdev,dev,1);
	if(result)
		printk(KERN_INFO "Errror %d adding char device.",result);
	printk(KERN_INFO "Char device registered.\n");
	return 0;
}

static void __exit null_exit(void)
{
	dev_t devno=MKDEV(null_major,null_minor);
	if(cdev)
		cdev_del(cdev);
	unregister_chrdev_region(devno,number_of_devices);
	printk(KERN_INFO "Char devices cleaned up.\n ");
}
module_init(null_init);
module_exit(null_exit);

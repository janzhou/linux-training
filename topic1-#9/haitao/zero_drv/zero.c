#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>
MODULE_LICENSE("GPL");

int zero_major=102;

int zero_minor=0;
int number_of_devices=1;

struct cdev *cdev;
dev_t dev=0;

static int zero_open(struct inode *inode,struct file *file)
{
	printk(KERN_INFO "My_zero device opened!\n");
	return 0;
}

ssize_t zero_read(struct file *file,char *buff, size_t len, loff_t *pOff)
{
	char *buffer="";
	int i;
	for(i=0;i<len;i++)
	{
		copy_to_user(buff,buffer,1);
	}
        return len;
}
 
ssize_t zero_write(struct file *file,const char *buff,size_t len,loff_t *pOff)
{
	//do nothing.
	return 0;

	
}

static int zero_release(struct inode *inode, struct file *file)
{
	printk(KERN_INFO "zero device closed!\n");
	return 0;
}

struct file_operations zero_fops=
{
	.owner=THIS_MODULE,
	.open=zero_open,
	.read=zero_read,
	.write=zero_write,
	.release=zero_release,
};

static int __init zero_init(void)
{
	int result;
	dev=MKDEV(zero_major,zero_minor);	
	result=register_chrdev_region(dev,number_of_devices,"zero_drv");
	if(result<0)
	{
		printk(KERN_WARNING "Cannot get major number %d\n",zero_major);
	}

	cdev=cdev_alloc();
	cdev_init(cdev,&zero_fops);
	cdev->owner=THIS_MODULE;
	cdev->ops=&zero_fops;
	result=cdev_add(cdev,dev,1);
	if(result)
		printk(KERN_INFO "Errror %d adding char device.",result);
	printk(KERN_INFO "Char device registered.\n");
	return 0;
}

static void __exit zero_exit(void)
{
	dev_t devno=MKDEV(zero_major,zero_minor);
	if(cdev)
		cdev_del(cdev);
	unregister_chrdev_region(devno,number_of_devices);
	printk(KERN_INFO "Char devices cleaned up.\n ");
}
module_init(zero_init);
module_exit(zero_exit);





#include<linux/module.h>
#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/fs.h>
#include<linux/types.h>
#include<asm/uaccess.h>
#include<linux/cdev.h>
MODULE_LICENSE ("GPL");

int zero_major = 110;
struct cdev zero_cdev;
#define CDEV_NAME "zerodev"
char *buffer="";
//open
static int zero_open(struct inode *inode,struct file *filp)
{printk("zero driver open\n");
 return 0;
}
//release
static int
zero_release(struct inode *inode,struct file *filp)
{printk("zero driver released\n");
 return 0;
}
//write
static ssize_t
zero_write(struct file *filp, const char __user * buf, size_t count,
	     loff_t * pos)
{
  return count;
}
//read
static ssize_t
zero_read (struct file * filp, const char __user * buf, size_t count,
	    loff_t * pos)
{ int i;
   for(i=0;i<count;i++)
   copy_to_user(buf,buffer,1);
  return count;
}

struct file_operations zero_fops = {
 .open=zero_open,
 .release=zero_release,
 .read = zero_read,
  .write = zero_write,
  .owner = THIS_MODULE,
};

/*int
zero_init (void)
{
  if ((zero_major = register_chrdev (0, "zerodev", &zero_fops)) >= 0)
    {
      printk ("register success!\n");
      return 1;
    }
  else
    {
      printk ("register failed!\n");
      return 0;
    }
}

zero_cleanup (void)
{
  unregister_chrdev (zero_major, "zerodev");
}

module_init (zero_init);
module_exit (zero_cleanup);*/


int zero_init(void) {
	int result;
	dev_t devno = MKDEV(zero_major, 0);
	if(zero_major){
		result = register_chrdev_region(devno, 1, CDEV_NAME);
	}
	else{
		result = alloc_chrdev_region(&devno, 0, 1, CDEV_NAME);
		zero_major = MAJOR(devno);
	}
	if(result < 0){
		printk("Can't get major devno:%d \n",zero_major);
		return result;
	}
	cdev_init(&zero_cdev,&zero_fops);
	zero_cdev.owner=THIS_MODULE;
	zero_cdev.ops=&zero_fops;
	cdev_add(&zero_cdev,devno,1);
	printk("zero dev init: %d\n",zero_major);
         if(!result)return 1;
}

 zero_cleanup(void)
{
	cdev_del(&zero_cdev);
	printk("zero dev clean: %d\n",zero_major);
	unregister_chrdev_region(MKDEV(zero_major, 0), 1);
	
}
//module_init(zero_init);
//module_exit(zero_cleanup);

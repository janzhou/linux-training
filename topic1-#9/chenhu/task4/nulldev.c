#include<linux/module.h>
#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/fs.h>
#include<linux/types.h>
#include<asm/uaccess.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/cdev.h>
#include <linux/slab.h>
MODULE_LICENSE ("GPL");

int null_major = 108;
struct cdev null_cdev;
#define CDEV_NAME "nulldev"
//open
static int
 null_open(struct inode *inode,struct file *filp)
{printk("null driver open\n");
  return 0;

}
static int
 null_release(struct inode *inode,struct file *filp)
{printk("null driver released\n");
  return 0;

}

static ssize_t
null_write (struct file *filp, const char __user * buf, size_t count,
	     loff_t * pos)
{
  return count;
}

static ssize_t
null_read (struct file * filp, const char __user * buf, size_t count,
	    loff_t * pos)
{
  return 0;
}

struct file_operations null_fops = {
  .open=null_open,
  .release=null_release,
  .read = null_read,
  .write = null_write,
  .owner = THIS_MODULE,
};

/*int
null_init (void)
{
  if ((null_major = register_chrdev (0, "nulldev", &null_fops)) >= 0)
    {
      printk ("register success!\n");
      return 1;
    }
  else
    {
      printk ("register failed!\n");
      return 0;
    }
}

null_cleanup (void)
{
  unregister_chrdev (null_major, "nulldev");
}

module_init (null_init);
module_exit (null_cleanup);*/


 int
 null_init(void)
{
	int result;
	dev_t devno = MKDEV(null_major, 0);
	if(null_major){
		result = register_chrdev_region(devno, 1, CDEV_NAME);
	}
	else{
		result = alloc_chrdev_region(&devno, 0, 1, CDEV_NAME);
		null_major = MAJOR(devno);
	}
	if(result < 0){
		printk("Can't get major devno:%d \n",null_major);
		return result;
	}
	cdev_init(&null_cdev,&null_fops);
       null_cdev.owner=THIS_MODULE;
	null_cdev.ops=&null_fops;
	cdev_add(&null_cdev,devno,1);
        return result;
}


 null_cleanup(void)
{
	cdev_del(&null_cdev);
	unregister_chrdev_region(MKDEV(null_major, 0), 1);
	
}

//module_init(null_init);
//module_exit(null_cleanup);

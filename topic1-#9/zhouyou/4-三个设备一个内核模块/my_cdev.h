#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <asm/io.h>
#include <asm/system.h>
#include <asm/uaccess.h>

#define SUCCESS 0

//需要调用的三个设备的函数
int my_null_init();
void my_null_exit();
int my_zero_init();
void my_zero_exit();
int my_buff_init();
void my_buff_exit();




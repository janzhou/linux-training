#include "my_cdev.h"


//声明设备，这是本设备的名字，将会出现在/proc/devices
#define DEVICE_NAME "my_buff_dev"
 
// 在这个文件中，主设备号作为全局变量以便于这个设备在注册和释放的时候使用
static int Major;
static int Minor;

#ifndef MEMDEV_MAJOR
#define MEMDEV_MAJOR 249   //预设的mem的主设备号
#endif

#ifndef BUFF_SIZE
#define BUFF_SIZE 4096 //预设的buff大小
#endif

static int my_major = MEMDEV_MAJOR;

struct buff_dev { 
	struct cdev cdev;
	unsigned char buff[BUFF_SIZE];
} my_buff_dev;

/**********************************************************************/

//open()函数
//功能：无论一个进程何时试图去打开这个设备都会调用这个函数
static int my_buff_open(struct inode *inode, struct file *file)
{	
	Minor = MINOR(inode->i_rdev);
	Major = MAJOR(inode->i_rdev);
	
	//dev_t i_rdev; 设备文件的inode结构,该字段表示真正的设备编号
	printk("Open_Device my_buff: %d.%d\n", Major, Minor);
 
	return SUCCESS;
}

//release ( ) 函数
//功能： 当一个进程试图关闭这个设备特殊文件的时候调用这个函数
static int my_buff_release(struct inode *inode, struct file *file)
{
	printk("Release_Device my_buff: %d.%d\n", Major, Minor);
	return SUCCESS;
}

//read ( ) 函数
//功能：当一个进程已经打开此设备文件以后并且试图去读它的时候调用这个函数（把读出的数据放到这个缓冲区,缓冲区的长度,文件中的偏移）
// 读/dev/buff设备可以读任意个0，写入无反应
static ssize_t my_buff_read(struct file *file, char *buffer, size_t length, loff_t *offset) 
{
	printk("Read_Device my_buff: %d.%d\n", Major, Minor);
	
	unsigned long p = *offset;  
	unsigned int count = length;
	int ret = 0;
  
	 //判断读位置是否有效
	if( p >= BUFF_SIZE)                   
		return 0;
	if( count > BUFF_SIZE - p )  
		count = BUFF_SIZE - p;
		
	//读数据到用户空间。如果数据拷贝成功，则返回零；否则，返回没有拷贝成功的数据字节数
	if( copy_to_user( buffer,(void*)(my_buff_dev.buff + p),count) )  
		ret = -EFAULT;  
	else{  
		*offset += count;  
		ret = count;  
		printk( KERN_INFO "Read %u bytes(s) from %d\n,  my_buff", count, p );  
	}  
  
	return ret;  
}

//write ( )  函数
//功能：当试图将数据写入这个设备文件的时侯，这个函数被调用
static ssize_t my_buff_write(struct file *file, const char *buffer, size_t length, loff_t *offset) 	
{
	printk("Write_Device my_buff: %d.%d\n", Major, Minor);

	unsigned long p = *offset;  
	unsigned int count = length;
	int ret = 0;  
  
	//判断写位置是否有效
	if(p >= BUFF_SIZE)  
		return 0;  
	if( count > BUFF_SIZE - p )  
		count = BUFF_SIZE - p;  
  
	//判断写位置是否有效
	if( copy_from_user( my_buff_dev.buff + p , buffer, count) )  
		ret = -EFAULT;  
	else{  
		*offset += count;  
		ret = count;  
		printk(KERN_INFO "Write %u bytes(s) from %d\n, my_buff", count, p);  
	}  
  
	return ret;
}

//llseek ()函数
//功能：用来修改文件的当前读写位置，并将新位置作为（正的）返回值返回
//参数orig为对文件定位的起始地址，这个值可以为文件开头（SEEK_SET，0,当前位置(SEEK_CUR,1)，文件末尾(SEEK_END,2)）
static loff_t my_buff_llseek(struct file *file, loff_t offset, int orig)
{
	printk("llseek my_buff.\n");
        
	loff_t ret;  
	
	if( offset < 0 || offset > BUFF_SIZE) 
		ret = -EINVAL;   

    switch(orig){  
	case 0:     //SEEK_SET
		file->f_pos = offset;          //file->f_pos当前的读/写位置
		ret = file->f_pos;  
        break;  
		
	case 1:   //SEEK_CUR
		if((file->f_pos + offset) > BUFF_SIZE){  
			ret = -EINVAL;  
			break;  
        }  
		
        file->f_pos += offset;  
        ret = file->f_pos;  
		break;   

	default:    
		ret = -EINVAL;  
    }  

	return ret;  		
}

//这个设备驱动程序提供给文件系统的接口
//当一个进程试图对我们生成的设备进行操作的时候就利用下面这个结构，这个结构就是我们提供给操作系统的接口，它的指针保存在设备表中,在init_module（）中被传递给操作系统
 
struct file_operations my_buff_fops = {
	.owner = THIS_MODULE,
	.llseek = my_buff_llseek,
	.read = my_buff_read,
	.write = my_buff_write,
	.open = my_buff_open,
	.release = my_buff_release,
 };
 
 
//模块的初始化和模块的卸载
//这个函数用来初始化这个模块 —注册该字符设备
//init_module()函数调用module_register_chrdev，把设备驱动程序添加到内核的字符设备驱动程序表中，它返回这个驱动程序所使用的主设备号。
int my_buff_init()
{
	int result;
	int err;
 
	dev_t my_buff = MKDEV(my_major, 0);
	
	//静态申请设备号
	result = register_chrdev_region(my_buff, 1, "my_buff_dev");
 
	
	//静态分配失败，动态申请设备号
	if (result < 0)
	{
		result = alloc_chrdev_region(&my_buff, 0, 1, "my_buff_dev");
		my_major = MAJOR(my_buff);
	}  
   
   
	printk("alloc successfully my_buff:%d(MAJOR)\n", my_major);
	if (result < 0)
		return result;
	
	//初始化cdev结构
	cdev_init(&my_buff_dev.cdev, &my_buff_fops);        //使my_buff_dev与my_buff_fops联系起来
	my_buff_dev.cdev.owner = THIS_MODULE;               //owner成员表示谁拥有这个驱动程序，使“内核引用模块计数”加1；THIS_MODULE表示现在这个模块被内核使用，这是内核定义的一个宏
	my_buff_dev.cdev.ops = &my_buff_fops;
   
	//注册字符设备 
	err = cdev_add(&my_buff_dev, my_buff, 1);
	if(err)
		printk(KERN_NOTICE "Error %d adding my_buff",err);  
	printk("add successfully my_buff.\n");
	
	return 0;

}
 
 
//这个函数的功能是卸载模块，主要是从/proc中取消注册的设备特殊文件。
void my_buff_exit()
{
	cdev_del(&my_buff_dev);                                        //注销设备
	unregister_chrdev_region(MKDEV(my_major,0),1);          //释放设备号
	printk("unregister successfully my_buff.\n");
}



#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#ifdef CONFIG_MODVERSIONS
#define MODVERSIONS
#include <linux/version.h>
#endif

int mydev_null_init(void);
int mydev_zero_init(void);
int mydev_temp_init(void);

void mydev_null_exit(void);
void mydev_zero_exit(void);
void mydev_temp_exit(void);
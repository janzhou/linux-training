#include <sys/types.h> 
#include <sys/stat.h> 
#include <string.h> 
#include <stdio.h> 
#include <fcntl.h> 

int main(void) 
{ 
  int fd,i; 
  char buf[1024]; 
  char get[1024]; 
  memset(get, 0, sizeof(get)); 
  memset(buf, 0, sizeof(buf)); 
  fd = open("/dev/mydev_zero", O_RDWR, S_IRUSR|S_IWUSR);
  
  if (fd > 0) { 
	
    if(read(fd, &buf, sizeof(buf)) < 0){ 
		printf("read end of File\n");
	}else{
		for(i=0;i<sizeof(buf);++i){
			if(buf[i] != 0)
				printf("buf[%d]=%c\n",i,buf[i]);
		}
		printf("The message in mydevice_null now is: %s\n", buf); 
	}

	printf("please enter a string you want input to mydriver:\n"); 
	scanf("%s",get); 
    if(write(fd, &get, sizeof(get))){
		printf("write success\n");
	}else{
		printf("write error\n");
	}	

    if(read(fd, &buf, sizeof(buf)) < 0){ 
		printf("read end of File\n");
	}else{
		for(i=0;i<sizeof(buf);++i){
			if(buf[i] != 0)
				printf("buf[%d]=%c\n",i,buf[i]);
		}
		printf("The message changed to: %s\n", buf);  
	}
    sleep(1); 
  }else { 
    printf("Open device error!\n"); 
    return -1; 
  } 
  close(fd);
  return 0; 
} 